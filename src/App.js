import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
});

var workId;
var breakId;

class App extends Component {
  state = {
    lastTimming: new Date(),
    totalWork: 0,
    totalBreak: 0,
    prosess: false,
    break: false,
    strWork: "",
    strBreak: "",
    firstStart: true,
  };

  countWork = () => {
    let pushedTime = new Date();
    let duringTime = pushedTime - this.state.lastTimming;
    this.setState({
      lastTimming: new Date(),
      totalWork: this.state.process ? this.state.totalWork + duringTime : this.state.totalWork
    })
  };

  countBreak = () => {
    let breakTime = new Date();
    let duringTime = breakTime - this.state.lastTimming;
    this.setState({
      lastTimming: new Date(),
      totalBreak: this.state.break ? this.state.totalBreak + duringTime : this.state.totalBreak,
    })
  };

  changeProcess = () => {
    let pushedTime = new Date();
    if(this.state.firstStart){
      this.setState({
        firstStart: false,
        lastTimming: new Date(),
      })
    }

    this.setState({
      process: this.state.process ? false : true,
      strWork: this.state.strWork + (new Date(pushedTime)).getHours() + "時" + (new Date(pushedTime)).getMinutes() + "分" + (this.state.process ? "¥n" : "〜"),
    })

    this.state.process ? clearInterval(workId) : workId = setInterval(this.countWork, 1000)
    clearInterval(breakId)
  }

  changeBreak = () => {
    let breakTime = new Date();
    this.setState({
      break: this.state.break ? false : true,
      strBreak: this.state.strBreak + (new Date(breakTime)).getHours() + "時" + (new Date(breakTime)).getMinutes() + "分" + (this.state.break ? "¥n" : "〜"),
    })

    this.state.break ? clearInterval(breakId) : breakId = setInterval(this.countBreak, 1000)
    this.state.break ? workId = setInterval(this.countWork, 1000) : clearInterval(workId)
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">作業時間管理</h1>
          <h2>現在の状態：
            {this.state.firstStart ? "作業開始前"
              : this.state.process ?
                this.state.break ? "休憩中" : "作業中"
            : "作業終了後"}
          </h2>
          <h3>【作業】{(new Date(this.state.totalWork)).getUTCHours()}時間{(new Date(this.state.totalWork)).getUTCMinutes()}分{(new Date(this.state.totalWork)).getUTCSeconds()}秒
          【休憩】{(new Date(this.state.totalBreak)).getUTCHours()}時間{(new Date(this.state.totalBreak)).getUTCMinutes()}分{(new Date(this.state.totalBreak)).getUTCSeconds()}秒</h3>
        </header>
        <p>
          <Button variant="contained" color="primary" className={classes.button} disabled={this.state.break} onClick={this.changeProcess}>
            {this.state.process ? "作業終了" : "作業開始"}
          </Button>
          <Button variant="contained" color="secondary" className={classes.button} disabled={!this.state.process} onClick={this.changeBreak}>
            {this.state.break ? "休憩終了" : "休憩開始"}
          </Button>
        </p>
        <hr/>
        <p>です。<br/>本日の作業が終了しました。</p>
        <p>【作業】合計：{(new Date(this.state.totalWork)).getUTCHours()}時間{(new Date(this.state.totalWork)).getUTCMinutes()}分</p>
        {this.state.strWork.split("¥n").map(function(line) {
            return (<p>{line}</p>);
        })}
        <p>【休憩】合計：{(new Date(this.state.totalBreak)).getUTCHours()}時間{(new Date(this.state.totalBreak)).getUTCMinutes()}分</p>
        {this.state.strBreak.split("¥n").map(function(line) {
            return (<p>{line}</p>);
        })}
        <p>【作業内容】<br/>
        </p>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
